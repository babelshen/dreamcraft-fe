import { AxiosResponse } from 'axios';
import { IRegistrationForm } from 'types/IRegistrationForm';
import { apiCaller } from 'utilities/api-caller';

export const registrationApi = async (data: IRegistrationForm): Promise<AxiosResponse> =>
  apiCaller({
    method: 'POST',
    url: '/api/auth/registration',
    data: { ...data },
  });

export const loginApi = async (login: string, password: string): Promise<AxiosResponse> =>
  apiCaller({
    method: 'POST',
    url: '/api/auth/login',
    data: {
      login,
      password,
    },
  });

export const logOutApi = async (): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/auth/logout',
    method: 'POST',
  });
