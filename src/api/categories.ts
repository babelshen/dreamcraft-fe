import { AxiosResponse } from 'axios';
import { apiCaller } from 'utilities/api-caller';

export const getCategoriesApi = async (search: string = '', sort: string = ''): Promise<AxiosResponse> =>
  apiCaller({
    url: `/api/categories${search ? `?search=${search}` : ''}${sort ? `${sort}` : ''}`,
  });

export const getCategoryByIdApi = async (id: number, sort?: string): Promise<AxiosResponse> =>
  apiCaller({
    url: `/api/categories/${id}${sort || false}`,
  });
