import { AxiosResponse } from 'axios';
import { apiCaller } from 'utilities/api-caller';

export const getRandomCommentsApi = async (): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/comments',
  });

export const createCommentApi = async (category_id: number, text: string): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/comments',
    method: 'POST',
    data: {
      category_id,
      text,
    },
  });
