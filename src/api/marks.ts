import { AxiosResponse } from 'axios';
import { apiCaller } from 'utilities/api-caller';

export const createMarkForCategoryApi = async (category_id: number, mark: number): Promise<AxiosResponse> =>
  apiCaller({
    url: `/api/marks`,
    method: 'POST',
    data: {
      category_id,
      mark,
    },
  });

export const checkExistingMarkApi = async (id: number): Promise<AxiosResponse> =>
  apiCaller({
    url: `/api/marks/${id}`,
  });
