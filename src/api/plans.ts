import { AxiosResponse } from 'axios';
import { apiCaller } from 'utilities/api-caller';

export const getPlansApi = async (): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/plans',
  });

export const getPlanByIdApi = async (id: number): Promise<AxiosResponse> =>
  apiCaller({
    url: `/api/plans/${id}`,
  });
