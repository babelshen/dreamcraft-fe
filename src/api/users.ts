import { AxiosResponse } from 'axios';
import { TUpdateMe } from 'types/TUpdateMe';
import { apiCaller } from 'utilities/api-caller';

export const getMeApi = async (): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/users/me',
  });

export const getUserById = async (id: number): Promise<AxiosResponse> =>
  apiCaller({
    url: `/api/users/${id}`,
  });

export const subscriptionToPlan = async (plan_id: number): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/users/subscribe',
    method: 'PATCH',
    data: {
      plan_id,
    },
  });

export const cancelSubscriptionToPlanApi = async (): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/users/cancel-plan',
    method: 'PATCH',
  });

export const choosePlanApi = async (category_id: number): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/users/choose-category',
    method: 'PATCH',
    data: {
      category_id,
    },
  });

export const changeMeApi = async (data: TUpdateMe): Promise<AxiosResponse> =>
  apiCaller({
    url: '/api/users/change-me',
    method: 'PATCH',
    data: { ...data },
  });
