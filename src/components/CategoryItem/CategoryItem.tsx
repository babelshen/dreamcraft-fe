import { Link } from 'react-router-dom';
import { ICategoryItem } from 'types/ICategoryItem';
import RateDisplay from 'ui/RateDisplay';
import PopularityBlockDisplay from 'ui/PopularityBlockDisplay';
import style from './CategoryItem.module.scss';

const CategoryItem: React.FC<ICategoryItem> = ({ category }) => (
  <Link to={`/categories/${category.id}`} className={style.category}>
    <div className={style.category__image_wrapper}>
      <img className={style.category__image} src={category.image} alt={category.title} title={category.title} />
    </div>
    <h3 className={style.category__title}>{category.title}</h3>
    <div className={style.category__statistic}>
      <RateDisplay mark={Number(category.marks_avg_mark)} />
      <PopularityBlockDisplay popularity={category.popularity} />
    </div>
  </Link>
);

export default CategoryItem;
