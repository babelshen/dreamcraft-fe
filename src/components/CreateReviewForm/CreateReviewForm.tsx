import { SubmitHandler, useForm } from 'react-hook-form';
import CustomTextarea from 'ui/CustomTextarea';
import { useParams } from 'react-router';
import SecondarySubmitButton from 'ui/SecondarySubmitButton';
import { ICreateReviewData } from 'types/ICreateReviewData';
import { createCommentApi } from 'api/comments';
import { TCreateReview } from 'types/TCreateReview';
import style from './CreateReviewForm.module.scss';

const CreateReviewForm: React.FC<TCreateReview> = ({ setIsSendReview }) => {
  const { id } = useParams();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ICreateReviewData>({
    mode: 'onSubmit',
    shouldUseNativeValidation: false,
  });

  const onSubmit: SubmitHandler<ICreateReviewData> = async (data) => {
    await createCommentApi(Number(id), data.review);
    reset();
    setIsSendReview((prev) => !prev);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={style.form}>
      <CustomTextarea
        rows={4}
        placeholder="Leave a review"
        id="review"
        error={errors.review}
        {...register('review', {
          required: 'This is a required field for submitting review',
          minLength: {
            value: 10,
            message: 'Your review cannot be less than 10 characters',
          },
          maxLength: {
            value: 500,
            message: 'Your review cannot be more than 500 characters',
          },
        })}
      />

      <SecondarySubmitButton>Post</SecondarySubmitButton>
    </form>
  );
};

export default CreateReviewForm;
