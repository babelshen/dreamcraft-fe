import NavigationLink from 'ui/NavigationLink';
import AccountButton from 'ui/AccountButton';
import LanguageButton from 'ui/LanguageButton';
import { navigationLinks } from './constants';
import style from './DesktopNavigation.module.scss';

const DesktopNavigation: React.FC = () => (
  <nav className={style.navigation}>
    {navigationLinks.map((linkItem) => (
      <NavigationLink key={linkItem.id} link={linkItem.link} title={linkItem.title} />
    ))}
    <AccountButton />
    <LanguageButton />
  </nav>
);

export default DesktopNavigation;
