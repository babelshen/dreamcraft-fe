import LanguageButton from 'ui/LanguageButton';
import NavigationLink from 'ui/NavigationLink';
import { useAppSelector } from 'utilities/hooks';
import { IUserStore } from 'types/IUserStore';
import { navigationLinks } from './constants';
import style from './FooterNavigation.module.scss';

const FooterNavigation: React.FC = () => {
  const { user } = useAppSelector((state) => state.user) as IUserStore;

  return (
    <nav className={style.navigation}>
      {navigationLinks.map((linkItem) => (
        <div>
          <NavigationLink key={linkItem.id} link={linkItem.link} title={linkItem.title} />
        </div>
      ))}
      <LanguageButton />
      {user.role === 'admin' ? <NavigationLink link="/admin" title="Admin Panel" /> : false}
    </nav>
  );
};

export default FooterNavigation;
