export const navigationLinks = [
  {
    id: 1,
    title: 'Dream Categories',
    link: '/categories',
  },
  {
    id: 2,
    title: 'Contacts',
    link: '/contact',
  },
  {
    id: 3,
    title: 'About',
    link: '/about',
  },
  {
    id: 4,
    title: 'Account',
    link: '/account',
  },
];
