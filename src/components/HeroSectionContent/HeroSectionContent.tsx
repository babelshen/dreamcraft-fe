import CustomButtonTypeButton from 'ui/CustomButtonTypeButton';
import { handleScroll } from 'utilities/handleScroll';
import { IRef } from 'types/IRef';
import style from './HeroSectionContent.module.scss';

const HeroSectionContent: React.FC<IRef> = ({ plansRef }) => (
  <div className={style.content}>
    <h1 className={style.content__title}>Reinvent your Sleep</h1>
    <h3 className={style.content__subtitle}>by connecting your BCI with DreamCraft and choosing the dream you will have</h3>
    <CustomButtonTypeButton onClick={() => handleScroll(plansRef)}>Plans</CustomButtonTypeButton>
  </div>
);

export default HeroSectionContent;
