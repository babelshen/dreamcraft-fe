import style from './HeroSectionImage.module.scss';

const HeroSectionImage: React.FC = () => <div className={style.image} />;

export default HeroSectionImage;
