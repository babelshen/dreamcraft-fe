import { useRef } from 'react';
import { Link } from 'react-router-dom';
import { navigationLinks } from 'components/DesktopNavigation/constants';
import { IMobileDeviceMenu } from 'types/IMobileDeviceMenu';
import AccountButton from 'ui/AccountButton';
import LanguageButton from 'ui/LanguageButton';
import style from './MobileDeviceMenu.module.scss';

const MobileDeviceMenu: React.FC<IMobileDeviceMenu> = ({ scroll, setScroll }) => {
  const overlayRef = useRef<HTMLDivElement | null>(null);
  const buttonRef = useRef<HTMLButtonElement | null>(null);

  const handleMobileClickMenu = () => {
    const overlayElement = overlayRef.current;
    const buttonElement = buttonRef.current;
    if (buttonElement) {
      buttonElement.classList.toggle(style.active);
    }
    if (overlayElement) {
      overlayElement.classList.toggle(style.open);
    }
    setScroll(!scroll);
    if (scroll) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  };

  const handleLinkClick = () => {
    const overlayElement = overlayRef.current;
    const buttonElement = buttonRef.current;

    if (buttonElement) {
      buttonElement.classList.toggle(style.active);
    }

    if (overlayElement) {
      overlayElement.classList.remove(style.open);
    }
    setScroll(false);
    document.body.style.overflow = 'auto';
  };

  return (
    <>
      <div className={style.nav__buttons}>
        <button type="button" className={style.nav__burger_wrapper} onClick={handleMobileClickMenu} ref={buttonRef}>
          <div className={style.nav__burger_first} />
          <div className={style.nav__burger_second} />
          <div className={style.nav__burger_third} />
        </button>
        <LanguageButton />
      </div>
      <div className={style.overlay} ref={overlayRef}>
        <nav className={style.mobile_menu}>
          <ul>
            {navigationLinks.map((link) => (
              <li key={link.id}>
                <Link to={link.link} className={style.nav__link} onClick={handleLinkClick}>
                  {link.title}
                </Link>
              </li>
            ))}
            <li>
              <button type="button" onClick={handleLinkClick} className={style.button__account}>
                <AccountButton />
              </button>
            </li>
          </ul>
        </nav>
      </div>
    </>
  );
};

export default MobileDeviceMenu;
