import CustomButtonSubmit from 'ui/CustomButtonSubmit';
import { IPlanItem } from 'types/IPlanItem';
import { useAppDispatch, useAppSelector } from 'utilities/hooks';
import { IUserStore } from 'types/IUserStore';
import { cancelSubscriptionToPlanApi, subscriptionToPlan } from 'api/users';
import { useEffect, useState } from 'react';
import { receiveMeReq } from 'store/user/action.creators';
import style from './PlanItem.module.scss';

const PlanItem: React.FC<IPlanItem> = ({ plan, index, length }) => {
  const [isChosePlan, setIsChosePlan] = useState<boolean>(false);
  const token = localStorage.getItem('userToken');

  const { user } = useAppSelector((state) => state.user) as IUserStore;
  const dispatch = useAppDispatch();

  const planSubscribe = (planId: number) => {
    if (user.plan_id && user.plan_id === plan.id) {
      cancelSubscriptionToPlanApi()
        .then(() => setIsChosePlan((prev) => !prev))
        .catch((e) => console.log(e));
    } else if (!user.plan_id) {
      subscriptionToPlan(planId)
        .then(() => setIsChosePlan((prev) => !prev))
        .catch((e) => console.log(e));
    }
  };

  useEffect(() => {
    dispatch(receiveMeReq());
  }, [dispatch, isChosePlan]);

  return (
    <article className={style[index === length - 1 ? 'plan-premium' : 'plan-general']}>
      <h3 className={style.plan__title}>{plan.title}</h3>
      <div className={style.plan__info}>
        <span className={style.plan__description}>{plan.description}</span>
        <span className={style.plan__price}>${plan.price}/month</span>
      </div>
      <span className={style.plan__restrictions}>{plan.restrictions}</span>

      {token ? (
        <div className={style.button__wrapper}>
          <CustomButtonSubmit
            chose = {plan.id === user.plan_id}
            onClick={() => {
              planSubscribe(plan.id);
            }}
          >
            {user.plan_id === plan.id ? 'Unsubscribe' : 'Subscribe'}
          </CustomButtonSubmit>
        </div>
      ) : (
        false
      )}
    </article>
  );
};
export default PlanItem;
