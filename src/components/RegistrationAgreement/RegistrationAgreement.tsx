import CustomCheckBox from 'ui/CustomCheckBox';
import { Link } from 'react-router-dom';
import { IRegistrationAgreement } from 'types/IRegistrationAgreement';
import style from './RegistrationAgreement.module.scss';

const RegistrationAgreement: React.FC<IRegistrationAgreement> = ({ isAgreement, setIsAgreement, isErrorAgreement }) => (
  <div className={style.agreement__wrapper}>
    <div className={style.agreement}>
      <CustomCheckBox isAgreement={isAgreement} setIsAgreement={setIsAgreement} />
      <span className={style.agreement__text}>
        I have read and agree with{' '}
        <Link to="/" className={style.agreement__link}>
          Privacy Policy
        </Link>{' '}
        and{' '}
        <Link to="/" className={style.agreement__link}>
          Terms of use
        </Link>
      </span>
    </div>
    <p className={style.agreement__error}>{isErrorAgreement}</p>
  </div>
);

export default RegistrationAgreement;
