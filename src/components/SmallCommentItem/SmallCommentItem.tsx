import { ICommentItem } from 'types/ICommentItem';
import SmallAvatar from 'ui/SmallAvatar';
import { Link } from 'react-router-dom';
import style from './SmallCommentItem.module.scss';

const SmallCommentItem: React.FC<ICommentItem> = ({ comment }) => (
  <article className={style.comment}>
    <div className={style.comment__user}>
      <SmallAvatar avatar={comment.avatar} nickname={comment.nickname} />
      <div className={style.comment__info}>
        <span className={style.comment__author}>{comment.nickname}</span>
        <Link to={`/categories/${comment.category_id}`} className={style.comment__category}>
          {comment.category_title}
        </Link>
      </div>
    </div>
    <div className={style.comment__text}>{comment.text}</div>
  </article>
);

export default SmallCommentItem;
