import { useState } from 'react';
import CustomRadioButton from 'ui/CustomRadioButton/CustomRadioButton';
import { useParams } from 'react-router-dom';
import { createMarkForCategoryApi } from 'api/marks';
import { TStarRating } from 'types/TStarRating';
import style from './StarRating.module.scss';
import { rating } from './constant';

const StarRating: React.FC<TStarRating> = ({ setIsSendMark }) => {
  const { id } = useParams();

  const [hoveredIndex, setHoveredIndex] = useState<number | null>(null);
  const [currentRate, setCurrentRate] = useState<number>(0);

  const handleStarHover = (index: number | null): void => {
    setHoveredIndex(index);
  };

  const sendMark = (mark: number) => {
    createMarkForCategoryApi(Number(id), mark)
      .then(() => setIsSendMark((prev) => !prev))
      .catch((e) => console.log(e));
  };
  return (
    <div className={style.rate__wrapper}>
      <div className={style.star__wrapper}>
        {rating.map((star, index) => (
          <CustomRadioButton
            key={star.value}
            handleStarHover={handleStarHover}
            setCurrentRate={setCurrentRate}
            value={star.value}
            index={index}
            currentRate={currentRate}
            hoveredIndex={hoveredIndex}
          />
        ))}
      </div>
      <button type="submit" className={style.rate__button} onClick={() => sendMark(currentRate)}>
        Rate
      </button>
    </div>
  );
};

export default StarRating;
