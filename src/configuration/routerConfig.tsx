import CategoriesListPage from 'pages/CategoriesListPage';
import CategoryPage from 'pages/CategoryPage';
import ErrorPage from 'pages/ErrorPage';
import HistoryPage from 'pages/HistoryPage';
import Layout from 'pages/Layout';
import LoginPage from 'pages/LoginPage';
import MainPage from 'pages/MainPage';
import PlansPage from 'pages/PlansPage';
import ProfileLayout from 'pages/ProfileLayout';
import RegistrationPage from 'pages/RegistrationPage';
import SettingsPage from 'pages/SettingsPage';
import { Route, createBrowserRouter, createRoutesFromElements } from 'react-router-dom';

const isAuth = localStorage.getItem('userToken');

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/">
      <Route
        index
        element={
          <Layout>
            <MainPage />
          </Layout>
        }
      />

      {isAuth ? (
        false
      ) : (
        <>
          <Route
            path="/login"
            element={
              <Layout>
                <LoginPage />
              </Layout>
            }
          />

          <Route
            path="/registration"
            element={
              <Layout>
                <RegistrationPage />
              </Layout>
            }
          />
        </>
      )}

      <Route
        path="/categories"
        element={
          <Layout>
            <CategoriesListPage />
          </Layout>
        }
      />

      <Route
        path="/categories/:id"
        element={
          <Layout>
            <CategoryPage />
          </Layout>
        }
      />

      {isAuth ? (
        <>
          <Route
            path="/account"
            element={
              <Layout>
                <ProfileLayout>
                  <PlansPage />
                </ProfileLayout>
              </Layout>
            }
          />

          <Route
            path="/history"
            element={
              <Layout>
                <ProfileLayout>
                  <HistoryPage />
                </ProfileLayout>
              </Layout>
            }
          />

          <Route
            path="/settings"
            element={
              <Layout>
                <ProfileLayout>
                  <SettingsPage />
                </ProfileLayout>
              </Layout>
            }
          />
        </>
      ) : (
        false
      )}

      <Route
        path="*"
        element={
          <Layout>
            <ErrorPage />
          </Layout>
        }
      />
    </Route>,
  ),
);

export default router;
