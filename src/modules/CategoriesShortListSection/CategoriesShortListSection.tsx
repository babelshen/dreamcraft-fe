import { useAppSelector } from 'utilities/hooks';
import SectionTitle from 'ui/SectionTitle';
import { ICategoryList } from 'types/ICategoryList';
import CategoryItem from 'components/CategoryItem/CategoryItem';
import CustomLink from 'ui/CustomLink';
import CustomLoader from 'ui/CustomLoader';
import ErrorLoadingData from 'ui/ErrorLoadingData';
import style from './CategoriesShortListSection.module.scss';

const CategoriesShortListSection: React.FC = () => {
  const categoriesStore = useAppSelector((state) => state.categories) as ICategoryList;
  const { categories } = categoriesStore;
  const count = window.innerWidth < 1025 ? 3 : 4;

  if (categoriesStore.loadingCategories || categoriesStore.successCategories)
    return (
      <section className={style.categories}>
        <SectionTitle>Wide range of dream categories</SectionTitle>
        <div className={style.categories__list}>
          {categoriesStore.successCategories ? categories.slice(0, count).map((category) => <CategoryItem key={category.id} category={category} />) : <CustomLoader />}
        </div>
        <CustomLink address="/categories">See all</CustomLink>
      </section>
    );
  return (
    <section className={style.categories}>
      <SectionTitle>Wide range of dream categories</SectionTitle>
      <ErrorLoadingData />
    </section>
  );
};

export default CategoriesShortListSection;
