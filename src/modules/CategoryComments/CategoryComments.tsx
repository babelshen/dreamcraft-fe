import CustomSelect from 'ui/CustomSelect';
import CreateReviewForm from 'components/CreateReviewForm';
import GeneralCommentItem from 'components/GeneralCommentItem';
import { ICategoryComments } from 'types/ICategoryComments';
import style from './CategoryComments.module.scss';

const CategoryComments: React.FC<ICategoryComments> = ({ comments, sortList, chosenSortOption, setChosenSortOption, setIsSendReview }) => {
  const token = localStorage.getItem('userToken');
  return (
    <section className={style.comments}>
      <div className={style.comments__title_wrapper}>
        <h3 className={style.comments__title}>Reviews</h3>
        <CustomSelect sortList={sortList} chosenSortOption={chosenSortOption} setChosenSortOption={setChosenSortOption} />
      </div>
      {token ? <CreateReviewForm setIsSendReview={setIsSendReview} /> : false}
      <div className={style.comments__wrapper}>
        {comments.map((comment) => (
          <GeneralCommentItem key={comment.id} comment={comment} />
        ))}
      </div>
    </section>
  );
};

export default CategoryComments;
