import StarRating from 'components/StarRating';
import RateDisplay from 'ui/RateDisplay';
import CustomButtonSubmit from 'ui/CustomButtonSubmit';
import { ICategoryDetailInfo } from 'types/ICategoryDetailInfo';
import CategoryImage from 'ui/CategoryImage';
import { useAppSelector } from 'utilities/hooks';
import { IUserStore } from 'types/IUserStore';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { checkExistingMarkApi } from 'api/marks';
import { choosePlanApi } from 'api/users';
import style from './CategoryDetailInfo.module.scss';

const CategoryDetailInfo: React.FC<ICategoryDetailInfo> = ({ categoryId, title, image, description, mark, popularity, setIsSendMark }) => {
  const { id } = useParams();

  const token = localStorage.getItem('userToken');

  const { user } = useAppSelector((state) => state.user) as IUserStore;

  const [isRated, setIsRated] = useState<boolean>(false);

  const choseCategory = async () => {
    await choosePlanApi(Number(id));
    window.location.reload();
  };

  useEffect(() => {
    checkExistingMarkApi(Number(id))
      .then((res) => setIsRated(res.data.data))
      .catch((e) => console.log(e));
  });

  return (
    <section className={style.section}>
      <h1 className={style.category__title}>{title}</h1>

      <div className={style.category__info}>
        <CategoryImage image={image} />
        <div className={style.category__info__content}>
          <div className={style.category__info__content__description}>{description}</div>
          <div className={style.category__info__content__rate}>
            <div className={style.category__info__content__statistic}>
              <div className={style.category__info__content__stars}>
                {token && !isRated ? <StarRating setIsSendMark={setIsSendMark} /> : false}
                <RateDisplay mark={Number(mark)} />
              </div>
              <span className={style.popularity}>Currently used by: {popularity}%</span>
            </div>
          </div>
          {token ? <CustomButtonSubmit onClick={choseCategory}>{user.category_id === categoryId ? 'UnChose' : 'Chose'}</CustomButtonSubmit> : false}
        </div>
      </div>
    </section>
  );
};

export default CategoryDetailInfo;
