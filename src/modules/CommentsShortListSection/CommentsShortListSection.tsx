import { getRandomCommentsApi } from 'api/comments';
import { useEffect, useState } from 'react';
import SectionTitle from 'ui/SectionTitle';
import { IComment } from 'types/IComment';
import SmallCommentItem from 'components/SmallCommentItem';
import ErrorLoadingData from 'ui/ErrorLoadingData';
import CustomLoader from 'ui/CustomLoader';
import style from './CommentsShortListSection.module.scss';

const CommentsShortListSection: React.FC = () => {
  const [comments, setComments] = useState<IComment[] | null>(null);
  const [isError, setIsError] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    getRandomCommentsApi()
      .then((result) => {
        setComments(result.data.data);
        setIsLoading(false);
      })
      .catch(() => {
        setIsError(true);
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return (
      <section className={style.comments}>
        <SectionTitle>What dreamcrafters say about us</SectionTitle>
        <CustomLoader />
      </section>
    );
  }

  if (isError) {
    return (
      <section className={style.comments}>
        <SectionTitle>What dreamcrafters say about us</SectionTitle>
        <ErrorLoadingData />
      </section>
    );
  }

  return (
    <section className={style.comments}>
      <SectionTitle>What dreamcrafters say about us</SectionTitle>
      <div className={style.comments__list}>{comments ? comments.map((comment) => <SmallCommentItem key={comment.id} comment={comment} />) : <p>Loading data...</p>}</div>
    </section>
  );
};

export default CommentsShortListSection;
