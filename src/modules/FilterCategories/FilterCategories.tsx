import CustomSearchInput from 'ui/CustomSearchInput';
import CustomSelect from 'ui/CustomSelect';
import { IFilterCategories } from 'types/IFilterCategories';
import { sortList } from 'modules/FilterCategories/constant';
import style from './FilterCategories.module.scss';

const FilterCategories: React.FC<IFilterCategories> = ({ searchValue, handleChange, handleReset, chosenSortOption, setChosenSortOption }) => (
  <div className={style.section__filter}>
    <CustomSearchInput searchValue={searchValue} handleChange={handleChange} handleReset={handleReset} placeholder="Search" />

    <CustomSelect sortList={sortList} chosenSortOption={chosenSortOption} setChosenSortOption={setChosenSortOption} />
  </div>
);

export default FilterCategories;
