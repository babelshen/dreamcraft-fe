export const sortList = [
  {
    id: 1,
    sortName: 'Most popular',
    sortPath: '?sort=desc',
  },
  {
    id: 2,
    sortName: 'Least popular',
    sortPath: '?sort=asc',
  },
  {
    id: 3,
    sortName: 'Most ranked',
    sortPath: '?mark-sort=desc',
  },
  {
    id: 4,
    sortName: 'Least ranked',
    sortPath: '?mark-sort=asc',
  },
  {
    id: 5,
    sortName: 'Alphabet',
    sortPath: '?alphabet',
  },
];
