import Logo from 'ui/Logo';
import FooterNavigation from 'components/FooterNavigation/FooterNavigation';
import style from './Footer.module.scss';

const Footer: React.FC = () => (
  <footer className={style.footer}>
    <div className={style.footer__wrapper}>
      <div className={style.footer__actions}>
        <Logo />
        <FooterNavigation />
      </div>
      <div className={style.footer__copyrights}>
        <p className={style.footer__copyrights_text}>Copyright © 2024 DreamCraft</p>
      </div>
    </div>
  </footer>
);

export default Footer;
