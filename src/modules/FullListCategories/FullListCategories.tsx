import CategoryItem from 'components/CategoryItem/CategoryItem';
import { ICategory } from 'types/ICategory';
import { PropsWithChildren } from 'react';
import CustomLoader from 'ui/CustomLoader';
import { useAppSelector } from 'utilities/hooks';
import { ICategoryList } from 'types/ICategoryList';
import ErrorLoadingData from 'ui/ErrorLoadingData';
import style from './FullListCategories.module.scss';

const FullListCategories: React.FC<PropsWithChildren<{ categoriesList: ICategory[]}>> = ({ categoriesList }) => {

  const categoryStore = useAppSelector((state) => state.categories) as ICategoryList;

  if (categoryStore.loadingCategories) return <CustomLoader />;
  if (categoryStore.successCategories && categoriesList.length) return (
    <div className={style.categories}>
       {categoriesList.map((category) => (
         <CategoryItem key={category.id} category={category} />
        ))}
     </div>
  )

  if (categoryStore.errorState!.code) return <ErrorLoadingData />

  return <div className={style.error__wrapper}>Categories not found!</div>;
}

export default FullListCategories;
