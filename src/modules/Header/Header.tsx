import DesktopNavigation from 'components/DesktopNavigation';
import Logo from 'ui/Logo';
import { useState } from 'react';
import MobileDeviceMenu from 'components/MobileDeviceMenu';
import style from './Header.module.scss';

const Header: React.FC = () => {
  const width = window.innerWidth;
  const [scroll, setScroll] = useState<boolean>(true);

  return (
    <header className={style.header}>
      <div className={style.wrapper}>
        <Logo />
        {width > 1024 ? <DesktopNavigation /> : <MobileDeviceMenu scroll={scroll} setScroll={setScroll} />}
      </div>
    </header>
  );
};

export default Header;
