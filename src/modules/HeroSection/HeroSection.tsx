import { IRef } from 'types/IRef';
import HeroSectionContent from 'components/HeroSectionContent';
import HeroSectionImage from 'components/HeroSectionImage';
import style from './HeroSection.module.scss';

const HeroSection: React.FC<IRef> = ({ plansRef }) => (
  <section className={style.hero}>
    <HeroSectionContent plansRef={plansRef} />
    <HeroSectionImage />
  </section>
);

export default HeroSection;
