import { SubmitHandler, useForm } from 'react-hook-form';
import CustomInput from 'ui/CustomInput';
import CustomButtonSubmit from 'ui/CustomButtonSubmit';
import Separator from 'ui/Separator';
import { Link, useNavigate } from 'react-router-dom';
import { ILogin } from 'types/ILogin';
import { useAppDispatch, useAppSelector } from 'utilities/hooks';
import { loginUserReq } from 'store/auth/action.creators';
import { IErrorPayload } from 'types/IErrorPayload';
import ErrorMessage from 'ui/ErrorMessage';
import { useEffect } from 'react';
import style from './LoginForm.module.scss';

const LoginForm: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const store = useAppSelector((state) => state.auth);

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ILogin>({
    mode: 'onSubmit',
    shouldUseNativeValidation: false,
  });

  const onSubmit: SubmitHandler<ILogin> = (data) => {
    dispatch(loginUserReq({ ...data }));
  };

  useEffect(() => {
    if (store.successLogin) {
      reset();
      navigate('/', { replace: true });
    }
  }, [navigate, reset, store.successLogin]);

  const loginError = store.errorState as IErrorPayload;

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={style.form}>
      <fieldset className={style.form__wrapper}>
        <legend className={style.form__title}>Welcome, DreamCrafter!</legend>
        <CustomInput
          type="text"
          id="login"
          label="Login"
          placeholder="Email or nickname"
          error={errors.login}
          {...register('login', {
            required: 'This is a required field to fill in',
          })}
        />

        <CustomInput
          type="password"
          id="password"
          label="Password"
          placeholder="Password"
          error={errors.password}
          {...register('password', {
            required: 'This is a required field to fill in',
            minLength: {
              value: 6,
              message: 'This field must contain at least 6 characters',
            },
            maxLength: {
              value: 20,
              message: 'This field should contain no more than 20 characters',
            },
          })}
        />

        <ErrorMessage error={loginError} />

        <CustomButtonSubmit onClick={handleSubmit(onSubmit)}>{store.loadingLogin ? <>Loading...</> : <>Login</>}</CustomButtonSubmit>

        <Separator />

        <p className={style.form__registration}>
          Don&apos;t have an account?{' '}
          <Link className={style.link} to="/registration">
            Sign up
          </Link>
        </p>
      </fieldset>
    </form>
  );
};

export default LoginForm;
