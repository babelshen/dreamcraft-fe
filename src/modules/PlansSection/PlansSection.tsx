import SectionTitle from 'ui/SectionTitle';
import { useAppSelector } from 'utilities/hooks';
import { IRef } from 'types/IRef';
import { IPlansList } from 'types/IPlansList';
import PlanItem from 'components/PlanItem/PlanItem';
import CustomLoader from 'ui/CustomLoader';
import ErrorLoadingData from 'ui/ErrorLoadingData';
import style from './PlansSection.module.scss';

const PlansSection: React.FC<IRef> = ({ plansRef }) => {
  const plansStore = useAppSelector((state) => state.plans) as IPlansList;
  const { plans } = plansStore;
  const lengthPlansList = plans.length;

  if (plansStore.loadingPlans || plansStore.successPlans)
    return (
      <section className={style.plans} ref={plansRef}>
        <SectionTitle>Plans</SectionTitle>
        <div className={style.plans__list}>{plans.length ? plans.map((plan, index) => <PlanItem key={plan.id} plan={plan} index={index} length={lengthPlansList} />) : <CustomLoader />}</div>
      </section>
    );

  return (
    <section className={style.plans} ref={plansRef}>
      <SectionTitle>Plans</SectionTitle>
      <ErrorLoadingData />
    </section>
  );
};

export default PlansSection;
