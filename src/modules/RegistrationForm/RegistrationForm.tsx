import { SubmitHandler, useForm } from 'react-hook-form';
import CustomButtonSubmit from 'ui/CustomButtonSubmit';
import CustomInput from 'ui/CustomInput';
import { useState } from 'react';
import RegistrationAgreement from 'components/RegistrationAgreement';
import { IRegistrationForm } from 'types/IRegistrationForm';
import { useNavigate } from 'react-router';
import { useAppDispatch, useAppSelector } from 'utilities/hooks';
import { registrationUserReq } from 'store/auth/action.creators';
import style from './RegistrationForm.module.scss';

const RegistrationForm: React.FC = () => {
  const [isAgreement, setIsAgreement] = useState<boolean>(false);
  const [isErrorAgreement, setIsErrorAgreement] = useState<string>('');

  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const store = useAppSelector((state) => state.auth);

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    getValues,
  } = useForm<IRegistrationForm>({
    mode: 'onSubmit',
    shouldUseNativeValidation: false,
  });

  const onSubmit: SubmitHandler<IRegistrationForm> = (data) => {
    if (isAgreement) {
      dispatch(registrationUserReq({ ...data }));
      setIsErrorAgreement('');
      reset();
      navigate('/login', { replace: true });
    } else {
      setIsErrorAgreement("You need to confirm that you're familiar with Private Policy and Terms of use");
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={style.form}>
      <fieldset className={style.form__wrapper}>
        <legend className={style.form__title}>Welcome, DreamCrafter!</legend>
        <div className={style.form__double_fields}>
          <CustomInput
            type="text"
            id="first_name"
            label="First name"
            placeholder="First name"
            error={errors.first_name}
            {...register('first_name', {
              required: 'This is a required field to fill in',
              minLength: {
                value: 2,
                message: 'This field must contain at least 2 characters',
              },
              maxLength: {
                value: 10,
                message: 'This field should contain no more than 10 characters',
              },
            })}
          />
          <CustomInput
            type="text"
            id="last_name"
            label="Last Name"
            placeholder="Last Name"
            error={errors.last_name}
            {...register('last_name', {
              required: 'This is a required field to fill in',
              minLength: {
                value: 2,
                message: 'This field must contain at least 2 characters',
              },
              maxLength: {
                value: 10,
                message: 'This field should contain no more than 10 characters',
              },
            })}
          />
        </div>

        <CustomInput
          type="text"
          id="nickname"
          label="Nickname"
          placeholder="Nickname"
          error={errors.nickname}
          {...register('nickname', {
            required: 'This is a required field to fill in',
            minLength: {
              value: 2,
              message: 'This field must contain at least 2 characters',
            },
            maxLength: {
              value: 15,
              message: 'This field should contain no more than 15 characters',
            },
          })}
        />

        <CustomInput
          type="email"
          id="email"
          label="Email"
          placeholder="Email"
          error={errors.email}
          {...register('email', {
            required: 'This is a required field to fill in',
            minLength: {
              value: 10,
              message: 'This field must contain at least 10 characters',
            },
            maxLength: {
              value: 64,
              message: 'This field should contain no more than 64 characters',
            },
          })}
        />

        <CustomInput
          type="password"
          id="password"
          label="Password"
          placeholder="Password"
          error={errors.password}
          {...register('password', {
            required: 'This is a required field to fill in',
            minLength: {
              value: 6,
              message: 'This field must contain at least 6 characters',
            },
            maxLength: {
              value: 20,
              message: 'This field should contain no more than 20 characters',
            },
            pattern: {
              value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$/,
              message: 'Your password must contain letters of different cases and numbers',
            },
            validate: (value) => {
              const confirmPasswordValue = getValues('password_confirmation');
              return value === confirmPasswordValue || 'Passwords do not match';
            },
          })}
        />

        <CustomInput
          type="password"
          id="password_confirmation"
          label="Confirm Password"
          placeholder="Password"
          error={errors.password_confirmation}
          {...register('password_confirmation', {
            required: 'This is a required field to fill in',
            minLength: {
              value: 6,
              message: 'This field must contain at least 6 characters',
            },
            maxLength: {
              value: 20,
              message: 'This field should contain no more than 20 characters',
            },
            pattern: {
              value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$/,
              message: 'Your password must contain letters of different cases and numbers',
            },
            validate: (value) => {
              const passwordValue = getValues('password');
              return value === passwordValue || 'Passwords do not match';
            },
          })}
        />

        <RegistrationAgreement isAgreement={isAgreement} setIsAgreement={setIsAgreement} isErrorAgreement={isErrorAgreement} />

        <CustomButtonSubmit onClick={handleSubmit(onSubmit)}>{store.loadingRegistration ? <>Loading...</> : <>Sign up</>}</CustomButtonSubmit>
      </fieldset>
    </form>
  );
};

export default RegistrationForm;
