import { useAppSelector } from 'utilities/hooks';
import { ICategoryList } from 'types/ICategoryList';
import { ChangeEvent, useEffect, useState } from 'react';
import { getCategoriesApi } from 'api/categories';
import { ICategory } from 'types/ICategory';
import FilterCategories from 'modules/FilterCategories';
import FullListCategories from 'modules/FullListCategories';
import { ISortOption } from 'types/ISortOption';
import { useDebounce } from 'utilities/useDebounce';
import style from './CategoriesListPage.module.scss';
import { sortList } from '../../modules/FilterCategories/constant';

const CategoriesListPage: React.FC = () => {
  const categoryStore = useAppSelector((state) => state.categories) as ICategoryList;
  const [chosenSortOption, setChosenSortOption] = useState<ISortOption>(sortList[0]);
  const [categoriesList, setCategoriesList] = useState<ICategory[]>([]);
  const [searchValue, setSearchValue] = useState<string>('');

  const debouncedSearch = useDebounce(searchValue, 500);


  useEffect(() => {
    if (categoryStore.successCategories) {
      setCategoriesList(categoryStore.categories);
    }
  }, [categoryStore.successCategories, categoryStore.categories]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
  };
  
  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const result = await getCategoriesApi(debouncedSearch, '');
        setCategoriesList(result.data.data);
      } catch (error) {
        console.error('Error fetching categories:', error);
      }
    };
  
    if (debouncedSearch !== '') {
      fetchCategories();
    } else {
      setCategoriesList(categoryStore.categories);
    }
  }, [categoryStore.categories, debouncedSearch]);
  useEffect(() => {
    getCategoriesApi('', chosenSortOption.sortPath)
      .then((result) => setCategoriesList(result.data.data))
      .catch((err) => console.log(err));
  }, [chosenSortOption]);

  const handleReset = () => {
    setSearchValue('');
    setCategoriesList(categoryStore.categories);
  };

  return (
    <section className={style.section}>
      <h1 className={style.section__title}>Dream categories</h1>

      <FilterCategories
        searchValue={searchValue}
        handleChange={handleChange}
        handleReset={handleReset}
        chosenSortOption={chosenSortOption}
        setChosenSortOption={setChosenSortOption}
        placeholder="Search"
      />

      <FullListCategories categoriesList={categoriesList} />
    </section>
  );
};

export default CategoriesListPage;
