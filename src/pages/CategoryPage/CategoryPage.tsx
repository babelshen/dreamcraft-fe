import { getCategoryByIdApi } from 'api/categories';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { ISortOption } from 'types/ISortOption';
import { ICategoryById } from 'types/ICategoryById';
import CategoryDetailInfo from 'modules/CategoryDetailInfo';
import CategoryComments from 'modules/CategoryComments';
import CustomLoader from 'ui/CustomLoader';
import ErrorLoadingData from 'ui/ErrorLoadingData';
import { sortList } from './constant';
import style from './CategoryPage.module.scss';

const CategoryPage: React.FC = () => {
  const { id } = useParams();
  const [category, setCategory] = useState<ICategoryById | null>(null);
  const [error, setError] = useState<boolean>(false);
  const [chosenSortOption, setChosenSortOption] = useState<ISortOption>(sortList[0]);
  const [isSendReview, setIsSendReview] = useState<boolean>(false);
  const [isSendMark, setIsSendMark] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await getCategoryByIdApi(Number(id), chosenSortOption.sortPath);
        setCategory(result.data.data);
        setIsSendReview(false);
        setIsSendMark(false);
      } catch (e) {
        setError(true);
      }
    };

    fetchData();
  }, [id, chosenSortOption, isSendReview, isSendMark]);

  if (category) {
    return (
      <>
        <CategoryDetailInfo
          title={category.title}
          image={category.image}
          description={category.description}
          mark={Number(category.marks_avg_mark)}
          popularity={category.popularity}
          setIsSendMark={setIsSendMark}
          categoryId={category.id}
        />

        <CategoryComments sortList={sortList} chosenSortOption={chosenSortOption} setChosenSortOption={setChosenSortOption} comments={category.comments} setIsSendReview={setIsSendReview} />
      </>
    );
  }
  if (error) return <ErrorLoadingData />;

  return (
    <div className={style.wrapper}>
      <CustomLoader />
    </div>
  );
};

export default CategoryPage;
