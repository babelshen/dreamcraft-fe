export const sortList = [
  {
    id: 1,
    sortName: 'Newest first',
    sortPath: '?sort=desc',
  },
  {
    id: 2,
    sortName: 'Oldest first',
    sortPath: '?sort=asc',
  },
];
