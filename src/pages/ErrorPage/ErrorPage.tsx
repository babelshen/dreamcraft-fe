import CustomLink from 'ui/CustomLink';
import style from './ErrorPage.module.scss';

const ErrorPage: React.FC = () => (
  <div className={style.wrapper}>
    <div className={style.glitch}>404</div>
    <CustomLink address="/">Main Page</CustomLink>
  </div>
);

export default ErrorPage;
