import { useAppSelector } from 'utilities/hooks';
import { IUserStore } from 'types/IUserStore';
import style from './HistoryPage.module.scss';

const HistoryPage: React.FC = () => {
  const { user } = useAppSelector((state) => state.user) as IUserStore;
  return (
    <div className={style.history__container}>
      {user.history.map((historyItem, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={index} className={style.history__wrapper}>
          {historyItem.greeting && (
            <div className={style.history__item}>
              <p className={style.history__date}>{historyItem.added_at}</p>
              <span className={style.message}>{historyItem.greeting}</span>
            </div>
          )}
          {historyItem.plan && (
            <div className={style.history__item}>
              <p className={style.history__date}>{historyItem.added_at}</p>
              <span className={style.message}>
                You have subscribed to <span className={style.message__plan}>{historyItem.plan.title}</span> plan!
              </span>
            </div>
          )}
          {historyItem.category && (
            <div className={style.history__item}>
              <p className={style.history__date}>{historyItem.added_at}</p>
              <span className={style.message}>
                You have chosen the <span className={style.message__category}>{historyItem.category.title}</span> category!
              </span>
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default HistoryPage;
