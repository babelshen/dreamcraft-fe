import Header from 'modules/Header';
import Footer from 'modules/Footer/Footer';
import { ILayout } from 'types/ILayout';
import { useAppDispatch } from 'utilities/hooks';
import { getCategoriesReq } from 'store/categories/action.creators';
import { useEffect } from 'react';
import { getPlansReq } from 'store/plans/action.creators';
import { receiveMeReq } from 'store/user/action.creators';
import style from './Layout.module.scss';

const Layout: React.FC<ILayout> = ({ children }) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getCategoriesReq());
    dispatch(getPlansReq());
    dispatch(receiveMeReq());
  }, [dispatch]);

  return (
    <div className={style.wrapper}>
      <Header />
      <main className={style.main}>{children}</main>
      <Footer />
    </div>
  );
};

export default Layout;
