import LoginForm from 'modules/LoginForm';
import style from './LoginPage.module.scss';

const LoginPage: React.FC = () => (
  <section className={style.section}>
    <h1 className={style.title}>Account - Log in</h1>
    <LoginForm />
  </section>
);

export default LoginPage;
