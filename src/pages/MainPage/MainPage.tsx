import { useRef } from 'react';
import Separator from 'ui/Separator';
import HeroSection from 'modules/HeroSection';
import CategoriesShortListSection from 'modules/CategoriesShortListSection';
import CommentsShortListSection from 'modules/CommentsShortListSection/CommentsShortListSection';
import PlansSection from 'modules/PlansSection';

const MainPage: React.FC = () => {
  const plansRef = useRef<HTMLDivElement>(null);

  return (
    <>
      <HeroSection plansRef={plansRef} />
      <Separator />
      <CategoriesShortListSection />
      <Separator />
      <CommentsShortListSection />
      <Separator />
      <PlansSection plansRef={plansRef} />
    </>
  );
};

export default MainPage;
