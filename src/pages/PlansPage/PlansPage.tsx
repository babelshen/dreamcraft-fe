import { useAppSelector } from 'utilities/hooks';
import { useEffect, useState } from 'react';
import { getPlanByIdApi } from 'api/plans';
import { dateTransform } from 'utilities/dateTransform';
import { IPlan } from 'types/IPlan';
import PlanItem from 'components/PlanItem';
import { IPlansList } from 'types/IPlansList';
import { IUserStore } from 'types/IUserStore';
import { cancelSubscriptionToPlanApi } from 'api/users';
import CustomLoader from 'ui/CustomLoader';
import style from './PlansPage.module.scss';

const PlansPage: React.FC = () => {
  const { user } = useAppSelector((state) => state.user) as IUserStore;
  const { plans } = useAppSelector((state) => state.plans) as IPlansList;
  const [plan, setPlan] = useState<IPlan | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      if (user.plan_id) {
        const result = await getPlanByIdApi(user.plan_id);
        setPlan(result.data.data);
      } else {
        setPlan(null);
      }
    };
    fetchData();
  }, [user.plan_id, user]);

  const cancelPlan = async () => {
    await cancelSubscriptionToPlanApi();
    window.location.reload();
  };

  if (user)
    return (
      <>
        <article className={style.plan__wrapper}>
          <div className={style.plan__field}>
            <span className={style.plan__field__title}>Your current plan:</span>
            <span className={style[plan ? 'chose_plan' : 'neutral']}>{plan ? plan.title : 'No choose'}</span>
          </div>
          <div className={style.plan__field}>
            <span className={style.plan__field__title}>Subscription till:</span>
            <span className={style.plan__field__value}>{user.duration_plan ? dateTransform(user.duration_plan) : ''}</span>
          </div>
          <button type="submit" onClick={() => cancelPlan()} className={style.plan__cancel}>
            Cancel subscription
          </button>
        </article>

        <article className={style.plans__wrapper}>
          {plans.length ? plans.map((planItem, index) => <PlanItem key={planItem.id} plan={planItem} index={index} length={plans.length} />) : <CustomLoader />}
        </article>
      </>
    );
  return <CustomLoader />;
};

export default PlansPage;
