import { useAppSelector } from 'utilities/hooks';
import { IProfileLayout } from 'types/IProfileLayout';
import ProfileNavigation from 'ui/ProfileNavigation';
import { IUserStore } from 'types/IUserStore';
import CustomLoader from 'ui/CustomLoader';
import style from './ProfileLayout.module.scss';

const ProfileLayout: React.FC<IProfileLayout> = ({ children }) => {
  const userStore = useAppSelector((state) => state.user) as IUserStore;
  const { user } = userStore;

  if (user) {
    return (
      <>
        <h1 className={style.account__title}>
          Account - {user.first_name} {user.last_name}
        </h1>
        <div className={style.account__wrapper}>
          <ProfileNavigation />
          <section className={style.account__tabs}>{children}</section>
        </div>
      </>
    );
  }

  return <CustomLoader />;
};

export default ProfileLayout;
