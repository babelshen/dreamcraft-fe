import RegistrationForm from 'modules/RegistrationForm';
import style from './RegistrationPage.module.scss';

const RegistrationPage: React.FC = () => (
  <section className={style.section}>
    <h1 className={style.title}>Account - Sign up</h1>
    <RegistrationForm />
  </section>
);

export default RegistrationPage;
