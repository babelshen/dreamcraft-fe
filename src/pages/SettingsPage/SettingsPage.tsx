import { SubmitHandler, useForm } from 'react-hook-form';
import { IUserStore } from 'types/IUserStore';
import { useAppDispatch, useAppSelector } from 'utilities/hooks';
import CustomInput from 'ui/CustomInput';
import { TUpdateMe } from 'types/TUpdateMe';
import { changeMeApi } from 'api/users';
import { receiveMeReq } from 'store/user/action.creators';
import SecondarySubmitButton from 'ui/SecondarySubmitButton';
import { logOutApi } from 'api/auth';
import { useState } from 'react';
import { useNavigate } from 'react-router';
import LogOutButton from 'ui/LogOutButton/LogOutButton';
import style from './SettingsPage.module.scss';

const SettingsPage: React.FC = () => {
  const navigation = useNavigate();
  const dispatch = useAppDispatch();
  const { user } = useAppSelector((state) => state.user) as IUserStore;

  const [isLogOutProcess, setIsLogOutProcess] = useState<boolean>(false);

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<TUpdateMe>({
    mode: 'onSubmit',
    shouldUseNativeValidation: false,
  });

  const onSubmit: SubmitHandler<TUpdateMe> = (data) => {
    try {
      changeMeApi(data);
    } catch (e) {
      console.log(e);
    } finally {
      dispatch(receiveMeReq());
    }
  };

  const logOut = async () => {
    try {
      setIsLogOutProcess(true);
      await logOutApi();
      localStorage.removeItem('userToken');
      setIsLogOutProcess(false);
      navigation('/', { replace: true });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className={style.form}>
        <fieldset className={style.form__wrapper}>
          <div className={style.form__container}>
            <div className={style.avatar__wrapper}>
              <img src={user.avatar} className={style.avatar__image} alt={user.nickname} title={user.nickname} />
            </div>
            <div className={style.official__fields}>
              <CustomInput
                type="text"
                id="first_name"
                label="First name"
                placeholder="First name"
                error={errors.first_name}
                defaultValue={user.first_name}
                {...register('first_name', {
                  minLength: {
                    value: 2,
                    message: 'This field must contain at least 2 characters',
                  },
                  maxLength: {
                    value: 10,
                    message: 'This field should contain no more than 10 characters',
                  },
                })}
              />

              <CustomInput
                type="text"
                id="last_name"
                label="Last Name"
                placeholder="Last Name"
                error={errors.last_name}
                defaultValue={user.last_name}
                {...register('last_name', {
                  minLength: {
                    value: 2,
                    message: 'This field must contain at least 2 characters',
                  },
                  maxLength: {
                    value: 10,
                    message: 'This field should contain no more than 10 characters',
                  },
                })}
              />
            </div>
          </div>

          <CustomInput
            type="text"
            id="nickname"
            label="Nickname"
            placeholder="Nickname"
            error={errors.nickname}
            defaultValue={user.nickname}
            {...register('nickname', {
              minLength: {
                value: 2,
                message: 'This field must contain at least 2 characters',
              },
              maxLength: {
                value: 15,
                message: 'This field should contain no more than 15 characters',
              },
            })}
          />

          <CustomInput
            type="url"
            id="avatar"
            label="Avatar Link"
            placeholder=""
            defaultValue={user.avatar}
            error={errors.avatar}
            {...register('avatar', {
              minLength: {
                value: 10,
                message: 'This field must contain at least 10 characters',
              },
              maxLength: {
                value: 64,
                message: 'This field should contain no more than 64 characters',
              },
            })}
          />

          <CustomInput
            type="email"
            id="email"
            label="Email"
            placeholder="Email"
            defaultValue={user.email}
            error={errors.email}
            {...register('email', {
              minLength: {
                value: 10,
                message: 'This field must contain at least 10 characters',
              },
              maxLength: {
                value: 64,
                message: 'This field should contain no more than 64 characters',
              },
            })}
          />

          <CustomInput
            type="password"
            id="password"
            label="Password"
            placeholder="Password"
            error={errors.password}
            {...register('password', {
              minLength: {
                value: 6,
                message: 'This field must contain at least 6 characters',
              },
              maxLength: {
                value: 20,
                message: 'This field should contain no more than 20 characters',
              },
              pattern: {
                value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$/,
                message: 'Your password must contain letters of different cases and numbers',
              },
              validate: (value) => {
                const confirmPasswordValue = getValues('password_confirmation');
                return value === confirmPasswordValue || 'Passwords do not match';
              },
            })}
          />

          <CustomInput
            type="password"
            id="password_confirmation"
            label="Confirm Password"
            placeholder="Password"
            error={errors.password_confirmation}
            {...register('password_confirmation', {
              minLength: {
                value: 6,
                message: 'This field must contain at least 6 characters',
              },
              maxLength: {
                value: 20,
                message: 'This field should contain no more than 20 characters',
              },
              pattern: {
                value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$/,
                message: 'Your password must contain letters of different cases and numbers',
              },
              validate: (value) => {
                const passwordValue = getValues('password');
                return value === passwordValue || 'Passwords do not match';
              },
            })}
          />
          <div className={style.button__wrapper}>
            <SecondarySubmitButton size="large__button">Save</SecondarySubmitButton>
          </div>
        </fieldset>
      </form>

      <LogOutButton logOut={logOut} isLogOutProcess={isLogOutProcess} />
    </>
  );
};

export default SettingsPage;
