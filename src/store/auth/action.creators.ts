import { createAsyncThunk } from '@reduxjs/toolkit';
import { IRegistrationForm } from 'types/IRegistrationForm';
import { ILogin } from 'types/ILogin';
import { loginApi, registrationApi } from 'api/auth';

export const registrationUserReq = createAsyncThunk('/api/auth/registration', async ({ ...data }: IRegistrationForm, { rejectWithValue }) => {
  try {
    const response = await registrationApi(data);
    return response.data;
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const loginUserReq = createAsyncThunk('/api/auth/login', async ({ login, password }: ILogin, { rejectWithValue }) => {
  try {
    const { data } = await loginApi(login, password);
    localStorage.setItem('userToken', data.data.token);
    return {
      data,
    };
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error.response.data);
  }
});
