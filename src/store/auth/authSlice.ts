import { createSlice } from '@reduxjs/toolkit';
import { registrationUserReq, loginUserReq } from './action.creators';
import { IErrorPayload } from 'types/IErrorPayload';

const initialState = {
  loadingRegistration: false,
  errorRegistration: false,
  successRegistration: false,
  loadingLogin: false,
  successLogin: false,
  errorState: {},
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(registrationUserReq.pending, (state) => {
        state.loadingRegistration = true;
        state.errorRegistration = false;
        state.successRegistration = false;
      })
      .addCase(registrationUserReq.fulfilled, (state) => {
        state.loadingRegistration = false;
        state.successRegistration = true;
        state.errorRegistration = false;
      })
      .addCase(registrationUserReq.rejected, (state) => {
        state.loadingRegistration = false;
        state.successRegistration = false;
        state.errorRegistration = true;
      });
    builder
      .addCase(loginUserReq.pending, (state) => {
        state.loadingLogin = true;
        state.successLogin = false;
        state.errorState = {};
      })
      .addCase(loginUserReq.fulfilled, (state) => {
        state.loadingLogin = false;
        state.successLogin = true;
        state.errorState = {};
      })
      .addCase(loginUserReq.rejected, (state, action) => {
        state.loadingLogin = false;
        state.successLogin = false;
        state.errorState = action.payload as IErrorPayload;
      });
  },
});

export const {} = authSlice.actions;

export default authSlice.reducer;
