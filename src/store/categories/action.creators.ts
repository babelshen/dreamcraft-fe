import { createAsyncThunk } from '@reduxjs/toolkit';
import { getCategoriesApi } from 'api/categories';

export const getCategoriesReq = createAsyncThunk('/api/categories', async (_, { rejectWithValue }) => {
  try {
    const search = '';
    const response = await getCategoriesApi(search);
    return response.data;
  } catch (error) {
    return rejectWithValue(error);
  }
});
