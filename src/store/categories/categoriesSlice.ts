import { createSlice } from '@reduxjs/toolkit';
import { getCategoriesReq } from './action.creators';
import { IErrorPayload } from 'types/IErrorPayload';

const initialState = {
  loadingCategories: false,
  successCategories: false,
  categories: {},
  errorState: {},
};

const categoriesSlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getCategoriesReq.pending, (state) => {
        state.loadingCategories = true;
        state.successCategories = false;
        state.errorState = {};
        state.categories = {};
      })
      .addCase(getCategoriesReq.fulfilled, (state, action) => {
        state.loadingCategories = false;
        state.successCategories = true;
        state.categories = action.payload.data;
        state.errorState = {};
      })
      .addCase(getCategoriesReq.rejected, (state, action) => {
        state.loadingCategories = false;
        state.successCategories = false;
        state.errorState = action.payload as IErrorPayload;
        state.categories = {};
      });
  },
});

export const {} = categoriesSlice.actions;

export default categoriesSlice.reducer;
