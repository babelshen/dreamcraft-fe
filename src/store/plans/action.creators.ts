import { createAsyncThunk } from '@reduxjs/toolkit';
import { getPlansApi } from 'api/plans';

export const getPlansReq = createAsyncThunk('/api/plans', async (_, { rejectWithValue }) => {
  try {
    const response = await getPlansApi();
    return response.data;
  } catch (error) {
    return rejectWithValue(error);
  }
});
