import { createSlice } from '@reduxjs/toolkit';
import { IErrorPayload } from 'types/IErrorPayload';
import { getPlansReq } from './action.creators';

const initialState = {
  loadingPlans: false,
  successPlans: false,
  plans: {},
  errorState: {},
};

const plansSlice = createSlice({
  name: 'plans',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getPlansReq.pending, (state) => {
        state.loadingPlans = true;
        state.successPlans = false;
        state.errorState = {};
        state.plans = {};
      })
      .addCase(getPlansReq.fulfilled, (state, action) => {
        state.loadingPlans = false;
        state.successPlans = true;
        state.plans = action.payload.data;
        state.errorState = {};
      })
      .addCase(getPlansReq.rejected, (state, action) => {
        state.loadingPlans = false;
        state.successPlans = false;
        state.errorState = action.payload as IErrorPayload;
        state.plans = {};
      });
  },
});

export const {} = plansSlice.actions;

export default plansSlice.reducer;
