import { configureStore } from '@reduxjs/toolkit';
import authSlice from './auth/authSlice';
import categoriesSlice from './categories/categoriesSlice';
import plansSlice from './plans/plansSlice';
import userSlice from './user/userSlice';

export const store = configureStore({
  reducer: {
    auth: authSlice,
    categories: categoriesSlice,
    plans: plansSlice,
    user: userSlice,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
