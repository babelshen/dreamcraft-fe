import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { getMeApi } from 'api/users';

export const receiveMeReq = createAsyncThunk('/api/users/me', async (_, { rejectWithValue }) => {
  try {
    const { data } = await getMeApi();
    return data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error.response.data);
  }
});

export const logoutReq = createAction('LOGOUT');
