import { createSlice } from '@reduxjs/toolkit';
import { IErrorPayload } from 'types/IErrorPayload';
import { logoutReq, receiveMeReq } from './action.creators';

const initialState = {
  loadingUser: false,
  successUser: false,
  user: {},
  errorState: {},
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(receiveMeReq.pending, (state) => {
        state.loadingUser = true;
        state.successUser = false;
        state.errorState = {};
        state.user = {};
      })
      .addCase(receiveMeReq.fulfilled, (state, action) => {
        state.loadingUser = false;
        state.successUser = true;
        state.user = action.payload.data;
        state.errorState = {};
      })
      .addCase(receiveMeReq.rejected, (state, action) => {
        state.loadingUser = false;
        state.successUser = false;
        state.errorState = action.payload as IErrorPayload;
        state.user = {};
      });
    builder.addCase(logoutReq, () => {
      return initialState;
    });
  },
});

export const {} = userSlice.actions;

export default userSlice.reducer;
