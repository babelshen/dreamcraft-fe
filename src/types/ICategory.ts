export interface ICategory {
  id: number;
  created_at: Date;
  updated_at: Date;
  title: string;
  description: string;
  image: string;
  marks_avg_mark: string | null;
  popularity: number;
}
