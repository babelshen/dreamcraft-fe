import { ICategory } from './ICategory';

export interface ICommentInCategory {
  id: number;
  created_at: Date;
  updated_at: Date;
  text: string;
  is_edit: '0' | '1';
  category_id: number;
  user_id: number;
}

export interface ICategoryById extends ICategory {
  comments: ICommentInCategory[];
}
