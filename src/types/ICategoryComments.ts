import { Dispatch, SetStateAction } from 'react';
import { ICommentInCategory } from './ICategoryById';
import { ISortOption } from './ISortOption';

export interface ICategoryComments {
  comments: ICommentInCategory[];
  sortList: ISortOption[];
  chosenSortOption: ISortOption;
  setChosenSortOption: (param: ISortOption) => void;
  setIsSendReview: Dispatch<SetStateAction<boolean>>;
}
