import { Dispatch, SetStateAction } from 'react';

export interface ICategoryDetailInfo {
  title: string;
  image: string;
  description: string;
  mark: number;
  popularity: number;
  setIsSendMark: Dispatch<SetStateAction<boolean>>;
  categoryId: number;
}
