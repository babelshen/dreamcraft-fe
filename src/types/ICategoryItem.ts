import { ICategory } from './ICategory';

export interface ICategoryItem {
  category: ICategory;
}
