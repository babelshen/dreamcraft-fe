import { ICategory } from './ICategory';
import { IErrorPayload } from './IErrorPayload';

export interface ICategoryList {
  loadingCategories: boolean;
  successCategories: boolean;
  categories: ICategory[];
  errorState: IErrorPayload;
}
