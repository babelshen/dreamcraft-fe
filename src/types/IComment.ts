export interface IComment {
  id: number;
  created_at: Date;
  updated_at: Date;
  text: string;
  is_edit: '0' | '1';
  category_id: number;
  category_title: string;
  user_id: number;
  nickname: string;
  avatar: string;
}
