import { IComment } from './IComment';

export interface ICommentItem {
  comment: IComment;
}
