import { ReactNode } from 'react';

export interface ICustomButtonSubmit {
  children: ReactNode;
  onClick: () => void;
  chose?: boolean;
}
