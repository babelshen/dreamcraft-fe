import { ReactNode } from 'react';

export interface ICustomButtonTypeButton {
  children: ReactNode;
  onClick: () => void;
}
