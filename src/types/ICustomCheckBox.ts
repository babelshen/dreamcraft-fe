export interface ICustomCheckBox {
  isAgreement: boolean;
  setIsAgreement: (param: boolean) => void;
}
