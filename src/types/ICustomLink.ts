import { ReactNode } from 'react';

export interface ICustomLink {
  address: string;
  children: ReactNode;
}
