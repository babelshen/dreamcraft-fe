export interface ICustomRadioButton {
  handleStarHover: (param: number | null) => void;
  setCurrentRate: (param: number) => void;
  value: number;
  index: number;
  currentRate: number;
  hoveredIndex: number | null;
}
