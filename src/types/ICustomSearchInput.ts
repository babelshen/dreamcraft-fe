import { ChangeEvent } from 'react';

export interface ICustomSearchInput {
  searchValue: string;
  handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
  handleReset: () => void;
  placeholder: string;
}
