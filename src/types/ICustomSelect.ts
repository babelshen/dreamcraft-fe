import { ISortOption } from './ISortOption';

export interface ICustomSelect {
  sortList: ISortOption[];
  chosenSortOption: ISortOption;
  setChosenSortOption: (param: ISortOption) => void;
}
