import { ICustomInput } from './ICustomInput';

type TextAreaType = Omit<ICustomInput, 'type' | 'label'>;

export interface ICustomTextArea extends TextAreaType {
  rows: number;
}
