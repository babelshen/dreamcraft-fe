export interface IErrorPayload {
  success: boolean;
  message: string;
  code?: string;
}
