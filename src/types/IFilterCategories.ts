import { ICustomSearchInput } from './ICustomSearchInput';
import { ICustomSelect } from './ICustomSelect';

type IFilterSelect = Omit<ICustomSelect, 'sortList'>;

export interface IFilterCategories extends ICustomSearchInput, IFilterSelect {}
