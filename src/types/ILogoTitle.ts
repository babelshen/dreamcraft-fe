import { ReactNode } from 'react';

export interface ILogoTitle {
  children: ReactNode;
}
