export interface IPlan {
  id: number;
  title: string;
  duration: number;
  price: number;
  description: string;
  restrictions: string;
}
