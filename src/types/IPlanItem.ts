import { IPlan } from './IPlan';

export interface IPlanItem {
  plan: IPlan;
  index: number;
  length: number;
}
