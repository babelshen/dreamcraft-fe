import { IErrorPayload } from './IErrorPayload';
import { IPlan } from './IPlan';

export interface IPlansList {
  loadingPlans: boolean;
  successPlans: boolean;
  plans: IPlan[];
  errorState: IErrorPayload;
}
