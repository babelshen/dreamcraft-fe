import { ReactNode } from 'react';

export interface IProfileLayout {
  children: ReactNode;
}
