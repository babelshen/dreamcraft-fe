import { RefObject } from 'react';

export interface IRef {
  plansRef: RefObject<HTMLDivElement>;
}
