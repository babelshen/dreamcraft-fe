import { ICustomCheckBox } from './ICustomCheckBox';

export interface IRegistrationAgreement extends ICustomCheckBox {
  isErrorAgreement: string;
}
