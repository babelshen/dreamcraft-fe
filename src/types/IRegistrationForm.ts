export interface IRegistrationForm {
  first_name: string;
  last_name: string;
  nickname: string;
  email: string;
  password: string;
  password_confirmation: string;
}
