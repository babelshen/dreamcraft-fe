import { ReactNode } from 'react';

export interface ISecondarySubmitButton {
  children: ReactNode;
  size?: string;
}
