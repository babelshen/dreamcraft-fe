import { ReactNode } from 'react';

export interface ISectionTitle {
  children: ReactNode;
}
