export interface ISmallAvatar {
  avatar: string;
  nickname: string;
}
