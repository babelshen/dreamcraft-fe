export interface ISortOption {
  id: number;
  sortName: string;
  sortPath: string;
}
