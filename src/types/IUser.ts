export interface IUserHistory {
  greeting?: string;
  plan?: {
    title: string;
    description: string;
  };
  category?: {
    title: string;
  };
  added_at: string;
}

export interface IUser {
  id: number;
  created_at: Date;
  updated_at: Date;
  first_name: string;
  last_name: string;
  nickname: string;
  email: string;
  email_verified_at: Date;
  avatar: string;
  role: 'user' | 'admin';
  ban: '0' | '1';
  reason_ban: null | string;
  category_id: number | null;
  history: IUserHistory[];
  plan_id: number | null;
  start_plan: Date;
  duration_plan: Date;
}
