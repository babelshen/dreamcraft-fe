import { IErrorPayload } from './IErrorPayload';
import { IUser } from './IUser';

export interface IUserStore {
  loadingUser: boolean;
  successUser: boolean;
  user: IUser;
  errorState: IErrorPayload;
}
