import { ICategoryComments } from './ICategoryComments';

export type TCreateReview = Pick<ICategoryComments, 'setIsSendReview'>;
