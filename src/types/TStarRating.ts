import { ICategoryDetailInfo } from './ICategoryDetailInfo';

export type TStarRating = Pick<ICategoryDetailInfo, 'setIsSendMark'>;
