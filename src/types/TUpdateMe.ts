import { IRegistrationForm } from './IRegistrationForm';

interface IUpdateForm extends IRegistrationForm {
  avatar: string;
}

export type TUpdateMe = Partial<IUpdateForm>;
