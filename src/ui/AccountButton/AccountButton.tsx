import { Link } from 'react-router-dom';
import style from './AccountButton.module.scss';

const AccountButton: React.FC = () => {
  const token = localStorage.getItem('userToken');
  const address = token ? '/account' : '/login';

  return (
    <Link to={address} className={`${style.button} ${token ? '' : style.blink}`}>
      Account
    </Link>
  );
};

export default AccountButton;
