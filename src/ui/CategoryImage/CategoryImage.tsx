import { ICategoryImage } from 'types/ICategoryImage';
import style from './CategoryImage.module.scss';

const CategoryImage: React.FC<ICategoryImage> = ({ image }) => <div style={{ backgroundImage: `url(${image})` }} className={style.image} />;

export default CategoryImage;
