import { ICustomButtonSubmit } from 'types/ICustomButtonSubmit';
import style from './CustomButtonSubmit.module.scss';

const CustomButtonSubmit: React.FC<ICustomButtonSubmit> = ({ children, onClick, chose=false }) => (
  <button type="submit" className={style[chose ? 'button-chose' : 'button']} onClick={onClick}>
    <span className={style.button__text}>{children}</span>
  </button>
);

export default CustomButtonSubmit;
