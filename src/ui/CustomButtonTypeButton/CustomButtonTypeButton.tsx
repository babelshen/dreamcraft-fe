import { ICustomButtonTypeButton } from 'types/ICustomButtonTypeButton';
import style from './CustomButtonTypeButton.module.scss';

const CustomButtonTypeButton: React.FC<ICustomButtonTypeButton> = ({ children, onClick }) => (
  <button type="button" className={style.button} onClick={onClick}>
    {children}
  </button>
);

export default CustomButtonTypeButton;
