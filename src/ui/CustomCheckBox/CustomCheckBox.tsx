import image from 'assets/accept.svg';
import { ICustomCheckBox } from 'types/ICustomCheckBox';
import style from './CustomCheckBox.module.scss';

const CustomCheckBox: React.FC<ICustomCheckBox> = ({ isAgreement, setIsAgreement }) => {
  const handleClick = () => setIsAgreement(!isAgreement);
  return (
    <button onClick={handleClick} type="button" className={style.box}>
      {isAgreement ? <img src={image} alt="Accept Privacy Policy and Terms of use" title="Accept Privacy Policy and Terms of use" /> : false}
    </button>
  );
};

export default CustomCheckBox;
