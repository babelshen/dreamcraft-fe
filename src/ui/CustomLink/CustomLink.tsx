import { Link } from 'react-router-dom';
import { ICustomLink } from 'types/ICustomLink';
import style from './CustomLink.module.scss';

const CustomLink: React.FC<ICustomLink> = ({ address, children }) => (
  <Link className={style.link} to={address}>
    {children}
  </Link>
);

export default CustomLink;
