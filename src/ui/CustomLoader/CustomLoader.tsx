import style from './CustomLoader.module.scss';

const CustomLoader: React.FC = () => (
  <div className={style.load__wrapper}>
    <p className={style.load__text}>We load data</p>
    <div className={style.load} />
  </div>
);

export default CustomLoader;
