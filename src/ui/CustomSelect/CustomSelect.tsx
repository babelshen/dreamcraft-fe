import { useState } from 'react';
import { ICustomSelect } from 'types/ICustomSelect';
import { ISortOption } from 'types/ISortOption';
import style from './CustomSelect.module.scss';

const CustomSelect: React.FC<ICustomSelect> = ({ sortList, chosenSortOption, setChosenSortOption }) => {
  const [isOpenMenu, setIsOpenMenu] = useState<boolean>(false);

  const handleClickButton = (): void => {
    setIsOpenMenu(!isOpenMenu);
  };

  const handleClickOption = (sortItem: ISortOption): void => {
    setChosenSortOption(sortItem);
    setIsOpenMenu(!isOpenMenu);
  };

  return (
    <div className={style.sort__wrapper}>
      <button type="button" className={style.sort__button} onClick={() => handleClickButton()}>
        <svg width="30" height="28" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M24 28L18 21.7778H22.5V6.22222H18L24 0L30 6.22222H25.5V21.7778H30M0 24.8889V21.7778H15V24.8889M0 15.5556V12.4444H10.5V15.5556M0 6.22222V3.11111H6V6.22222H0Z" fill="white" />
        </svg>
      </button>

      {isOpenMenu ? (
        <div className={style.sort__options}>
          {sortList.map((sortItem) => (
            <button
              key={sortItem.id}
              type="submit"
              onClick={() => handleClickOption(sortItem)}
              className={style[sortItem.sortName === chosenSortOption.sortName ? 'sort__option-chosen' : 'sort__option']}
            >
              {sortItem.sortName}
            </button>
          ))}
        </div>
      ) : (
        false
      )}
    </div>
  );
};
export default CustomSelect;
