import style from './ErrorLoadingData.module.scss';

const ErrorLoadingData: React.FC = () => <div className={style.error__wrapper}>Something wrong. Try later</div>;

export default ErrorLoadingData;
