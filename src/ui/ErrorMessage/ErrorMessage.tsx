import { IErrorPayload } from 'types/IErrorPayload';
import style from './ErrorMessage.module.scss';

const ErrorMessage: React.FC<{ error?: IErrorPayload }> = ({ error }) => (error?.message ? <p className={style.error}>{error.message}</p> : false);

export default ErrorMessage;
