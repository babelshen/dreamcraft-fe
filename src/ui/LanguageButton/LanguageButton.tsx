import { useState } from 'react';
import { useAppDispatch } from 'utilities/hooks';
import { getCategoriesReq } from 'store/categories/action.creators';
import { getPlansReq } from 'store/plans/action.creators';
import style from './LanguageButton.module.scss';

const LanguageButton: React.FC = () => {
  const dispatch = useAppDispatch();

  const [isChangeLanguage, setIsChangeLanguage] = useState<boolean>(false);
  const handleChangeLanguage = () => {
    setIsChangeLanguage(!isChangeLanguage);
    if (isChangeLanguage) {
      localStorage.setItem('language', 'uk');
    } else {
      localStorage.removeItem('language');
    }
    dispatch(getCategoriesReq());
    dispatch(getPlansReq());
  };
  return (
    <button type="button" onClick={handleChangeLanguage} className={style.button}>
      {localStorage.getItem('language') ? 'UK' : 'EN'}
    </button>
  );
};

export default LanguageButton;
