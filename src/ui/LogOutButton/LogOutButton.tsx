import style from './LogOutButton.module.scss';

interface ILogOutButton {
  logOut: () => void;
  isLogOutProcess: boolean;
}

const LogOutButton: React.FC<ILogOutButton> = ({ logOut, isLogOutProcess }) => (
  <button type="button" className={style.logout} onClick={() => logOut()}>
    {isLogOutProcess ? 'In Process...' : 'Logout'}
  </button>
);

export default LogOutButton;
