import { Link } from 'react-router-dom';
import { INavigationLink } from 'types/INavigationLink';
import style from './NavigationLink.module.scss';

const NavigationLink: React.FC<INavigationLink> = ({ link, title }) => (
  <Link to={link} className={style.navigation_link}>
    {title}
  </Link>
);

export default NavigationLink;
