import { IPopularityBlockDisplay } from 'types/IPopularityBlockDisplay';
import style from './PopularityBlockDisplay.module.scss';

const PopularityBlockDisplay: React.FC<IPopularityBlockDisplay> = ({ popularity }) => <span className={style.popularity}>{`Currently used by: ${popularity} %`}</span>;

export default PopularityBlockDisplay;
