import { NavLink } from 'react-router-dom';
import style from './ProfileNavigation.module.scss';
import { links } from './constant';

const ProfileNavigation: React.FC = () => (
  <nav className={style.navigation__wrapper}>
    {links.map((link) => (
      <NavLink className={style.link} key={link.id} to={link.address}>
        {link.title}
      </NavLink>
    ))}
  </nav>
);

export default ProfileNavigation;
