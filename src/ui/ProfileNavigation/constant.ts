export const links = [
  {
    id: 1,
    title: 'Plans',
    address: '/account',
  },
  {
    id: 2,
    title: 'History',
    address: '/history',
  },
  {
    id: 3,
    title: 'Settings',
    address: '/settings',
  },
];
