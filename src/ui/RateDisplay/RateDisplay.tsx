import { IRateDisplay } from 'types/IRateDisplay';
import style from './RateDisplay.module.scss';

const RateDisplay: React.FC<IRateDisplay> = ({ mark }) => (
  <div className={style.wrapper}>
    <svg width="18" height="19" viewBox="0 0 18 19" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M9 15.5745L13.3649 18.3368C14.1642 18.843 15.1424 18.0947 14.932 17.1482L13.7751 11.9537L17.6351 8.45405C18.3398 7.81575 17.9611 6.60517 17.0356 6.52813L11.9555 6.07692L9.96763 1.16857C9.61003 0.277143 8.38997 0.277143 8.03237 1.16857L6.04451 6.06591L0.964434 6.51713C0.038871 6.59416 -0.339768 7.80474 0.364921 8.44305L4.22494 11.9427L3.06799 17.1372C2.85763 18.0837 3.83578 18.832 4.63513 18.3258L9 15.5745Z"
        fill="#FFA800"
      />
    </svg>
    <span className={style.rate}>{mark === 0 ? 'No rate yet' : mark}</span>
  </div>
);

export default RateDisplay;
