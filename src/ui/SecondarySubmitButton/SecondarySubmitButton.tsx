import { ISecondarySubmitButton } from 'types/ISecondarySubmitButton';
import style from './SecondarySubmitButton.module.scss';

const SecondarySubmitButton: React.FC<ISecondarySubmitButton> = ({ children, size = '' }) => (
  <button type="submit" className={style[size ? `${size}` : 'button']}>
    {children}
  </button>
);

export default SecondarySubmitButton;
