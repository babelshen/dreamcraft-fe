import { ISectionTitle } from 'types/ISectionTitle';
import style from './SectionTitle.module.scss';

const SectionTitle: React.FC<ISectionTitle> = ({ children }) => <h2 className={style.title}>{children}</h2>;

export default SectionTitle;
