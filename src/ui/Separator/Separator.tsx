import style from './Separator.module.scss';

const Separator: React.FC = () => <div className={style.separator} />;

export default Separator;
