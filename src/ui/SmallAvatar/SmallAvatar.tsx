import { ISmallAvatar } from 'types/ISmallAvatar';
import style from './SmallAvatar.module.scss';

const SmallAvatar: React.FC<ISmallAvatar> = ({ avatar, nickname }) => (
  <div className={style.avatar__wrapper}>
    <img src={avatar} className={style.avatar} alt={`Avatar of the ${nickname}`} title={`Avatar of the ${nickname}`} />
  </div>
);

export default SmallAvatar;
