import axios, { AxiosRequestConfig, AxiosResponse, RawAxiosRequestHeaders } from 'axios';

type ExpandedAxiosRequestConfig = AxiosRequestConfig & {
  tokenObj?: any;
  path?: string;
  onError?: any;
};

export const apiCaller = async (opts: ExpandedAxiosRequestConfig = {}): Promise<AxiosResponse> => {
  const { url = null, method = 'get', params = {}, data = null } = opts;
  const headers: RawAxiosRequestHeaders = {};
  headers['Content-Type'] = 'application/json';
  headers['Accept-Language'] = localStorage.getItem('language') || 'en';

  const token = localStorage.getItem('userToken');

  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }

  const requestURL = import.meta.env.VITE_API_ADDRESS + url;

  const config = {
    url: requestURL,
    method,
    data,
    headers,
    params,
  };

  const axiosApiInstance = axios.create(config);

  return axiosApiInstance.request(config);
};
