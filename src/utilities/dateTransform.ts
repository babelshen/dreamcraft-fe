export const dateTransform = (date: Date): string => {
  const postDate = new Date(date);
  const newDate = {
    day: postDate.getDate(),
    month: postDate.getMonth() + 1,
    year: postDate.getFullYear(),
  };

  return `${newDate.day}.${newDate.month < 10 ? `0${newDate.month}` : newDate.month}.${newDate.year}`;
};
