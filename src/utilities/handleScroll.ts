import { RefObject } from 'react';

export const handleScroll = (plansRef: RefObject<HTMLDivElement>): void => {
  if (plansRef.current) {
    plansRef.current.scrollIntoView({ behavior: 'smooth' });
  }
};
