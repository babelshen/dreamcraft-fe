import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      api: "/src/api",
      components: "/src/components",
      config: "/src/configuration",
      pages: "/src/pages",
      store: "/src/store",
      styles: "/src/styles",
      assets: "/src/assets",
      types: "/src/types",
      ui: "/src/ui",
      utilities: "/src/utilities",
      modules: "/src/modules",
    }
  }
})
